#!/usr/bin/env python3

import argparse
import csv
from pytz import timezone
from icalendar import Calendar, Event, vText,vDatetime, Timezone, TimezoneStandard, TimezoneDaylight
from datetime import datetime, timedelta
from uuid import uuid4
from platform import uname

hDateLabel = "datum"
hStartLabel = "von"
hEndLabel = "bis"
hLocationLabel = "ort"
hSummaryLabel = "titel"
hDescriptionLabel = "text"
tz = "Europe/Berlin"

parser = argparse.ArgumentParser(description="Converts csv file to ics file.")
parser.add_argument("fileName", help="Name of the csv file")
args = parser.parse_args()

try:
    # read CSV file
    with open(args.fileName.strip(), 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')

        # Read header line
        fields = []
        for field in next(csvreader):
            fields.append(field.strip().lower())

        #Read csv data rows
        rows = []
        for row in csvreader:
            rows.append(row)

    # check headers
    hDate = fields.index(hDateLabel)
    hStart = fields.index(hStartLabel)
    hEnd = fields.index(hEndLabel)
    hLocation = fields.index(hLocationLabel)
    hSummary = fields.index(hSummaryLabel)
    hDescription = fields.index(hDescriptionLabel)

    # create calendar
    cal = Calendar()
    cal.add('prodid', '-//tho//https://codeberg.org/tho/csv-to-ics//')
    cal.add('version', '2.0')
    
    tzc = Timezone()
    tzc.add('tzid', tz)
    tzc.add('x-lic-location', tz)

    tzs = TimezoneStandard()
    tzs.add('tzname', 'CET')
    tzs.add('dtstart', datetime(1970, 10, 25, 3, 0, 0))
    tzs.add('rrule', {'freq': 'yearly', 'bymonth': 10, 'byday': '-1su'})
    tzs.add('TZOFFSETFROM', timedelta(hours=2))
    tzs.add('TZOFFSETTO', timedelta(hours=1))

    tzd = TimezoneDaylight()
    tzd.add('tzname', 'CEST')
    tzd.add('dtstart', datetime(1970, 3, 29, 2, 0, 0))
    tzs.add('rrule', {'freq': 'yearly', 'bymonth': 3, 'byday': '-1su'})
    tzd.add('TZOFFSETFROM', timedelta(hours=1))
    tzd.add('TZOFFSETTO', timedelta(hours=2))

    tzc.add_component(tzs)
    tzc.add_component(tzd)
    cal.add_component(tzc)

    for row in rows:
        date = str(row[hDate]).split('.')
        start = str(row[hStart]).split(':')
        end = str(row[hEnd]).split(':')

        event = Event()
        event.add('dtstart', vDatetime(datetime(int(date[2]),int(date[1]), int(date[0]), int(start[0]), int(start[1]), 0, 0, tzinfo=timezone(tz))))
        event.add('dtend', vDatetime(datetime(int(date[2]),int(date[1]), int(date[0]), int(end[0]), int(end[1]), 0, 0, tzinfo=timezone(tz))))
        event.add('location', vText(row[hLocation]))
        event.add('summary', vText(row[hSummary]))
        event.add('description', vText(row[hDescription]))
        event.add('dtstamp', vDatetime(datetime.now()))
        event.add('uid', uuid4().hex + '@' + uname().node)

        cal.add_component(event)

    # write ics file
    data = cal.to_ical()
    with open('out.ics', 'wb') as ical_file:
        ical_file.write(data)

except FileNotFoundError as err:
    print(str(err))
    quit()
except ValueError as err:
    print('Header field missing: ' + str(err))
    quit()
