# csv-to-ics

Convert a CSV file to an ICS file.

> Usage:
>
> `csv-to-ics.py fileName.csv`

It will create a file named `out.ics` in the same directory which can be imported into a calendar e.g. in NextCloud.

Known limitations:

- Only one time zone supported.
- No repeated appointments.
- No localisations of the CSV headers
- I am sure there is more ...

I searched the internet to find something like this but did not find any.

Hope it helps you.
